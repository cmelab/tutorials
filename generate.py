import mbuild as mb
from compounds import gen_n_alkane

def make_box(out_file="init.hoomdxml", compound=gen_n_alkane(), n_compounds=100, density=500):
    box = mb.fill_box(compound, n_compounds=n_compounds, density=density)
    box.save(out_file, forcefield_name="oplsaa", overwrite=True, combining_rule="lorentz", auto_scale=True)