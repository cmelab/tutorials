# TODO

First lets start with a simple n-alkane system and explore temp and n-space
These tutorials will be nicer once we finish the hoomd-mbuild intergration

1. initialize with mbuild

* have compounds use plugin stuff for mbuild

* update with new backend to write out hoomd json

1. atom type with foyer

* update to use custom foyer xml file

1. simulate with hoomd

* use metadata.json to read in ff params

* hoomd best practies (restart/continue jobs)

1. sweep with signac framework
