import mbuild as mb 

def gen_n_alkane(n=5):
    return mb.load("C"*n, smiles=True)