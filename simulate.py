from generate import make_box
from compounds import gen_n_alkane
import hoomd

make_box(out_file="init.gsd", compound=gen_n_alkane(n=2), n_compounds=100, density=500)

system = hoomd.